package sensilabs.pl.sensishopinglist.uilts;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import sensilabs.pl.sensishopinglist.pojos.Product;

public class SharedPrefUtil {

    public static void saveUser(Context context, List<Product> shopinglist) {
        saveObjectToSharedPreference(context, "shopingList", shopinglist);
    }

    public static List<Product> getProducts(Context context) {
        return getSavedObjectFromPreference(context, "shopingList", new TypeToken<ArrayList<Product>>(){}.getType());
    }

    private static void saveObjectToSharedPreference(Context context, String serializedObjectKey, Object object) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("pl.sensilabs", 0);
        SharedPreferences.Editor sharedPreferencesEditor = sharedPreferences.edit();
        final Gson gson = new Gson();
        String serializedObject = gson.toJson(object);
        sharedPreferencesEditor.putString(serializedObjectKey, serializedObject);
        sharedPreferencesEditor.apply();
    }

    private static <GenericClass> GenericClass getSavedObjectFromPreference(Context context, String preferenceKey, Type listType) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("pl.sensilabs", 0);
        if (sharedPreferences.contains(preferenceKey)) {
            final Gson gson = new Gson();
            return gson.fromJson(sharedPreferences.getString(preferenceKey, ""), listType);
        }
        return null;
    }

}

