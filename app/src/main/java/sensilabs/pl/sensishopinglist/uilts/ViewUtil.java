package sensilabs.pl.sensishopinglist.uilts;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

public class ViewUtil {

    private static FragmentManager getFragmentManager(AppCompatActivity activity) {
        return activity.getSupportFragmentManager();
    }

    public static void addFragment(AppCompatActivity activity, int fragment_place, Fragment fragment) {
        FragmentTransaction fragmentTransaction = getFragmentManager(activity).beginTransaction();
        fragmentTransaction.replace(fragment_place, fragment);
        fragmentTransaction.commit();
    }
}
