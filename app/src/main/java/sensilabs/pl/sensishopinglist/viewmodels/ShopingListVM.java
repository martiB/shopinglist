package sensilabs.pl.sensishopinglist.viewmodels;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.MutableLiveData;
import android.content.Context;
import android.os.Build;

import java.util.ArrayList;
import java.util.List;

import sensilabs.pl.sensishopinglist.pojos.Product;
import sensilabs.pl.sensishopinglist.uilts.SharedPrefUtil;

public class ShopingListVM extends AndroidViewModel {
    private MutableLiveData<List<Product>> productListLiveData;

    public ShopingListVM(Application application) {
        super(application);
        getProductsFromSharePref(application);
    }

    public void getProductsFromSharePref(Application application) {
        List<Product> products = SharedPrefUtil.getProducts(application);
        if (products != null) {
            getProductListLiveData().setValue(products);
        } else initEmptyLiveData();
    }

    public MutableLiveData<List<Product>> getProductListLiveData() {
        if (productListLiveData == null) {
            initEmptyLiveData();
        }
        return productListLiveData;
    }

    private void initEmptyLiveData() {
        productListLiveData = new MutableLiveData<>();
        productListLiveData.setValue(new ArrayList<>());
    }

    public void addProduct(Product product) {
        List<Product> productList = getProductListLiveData().getValue();
        if (productList != null) {
            productList.add(product);
            productListLiveData.setValue(productList);
        }
    }

    public void removeProduct(Product product) {
        List<Product> productList = getProductListLiveData().getValue();
        if (productList != null && product != null && productList.contains(product)) {
            productList.remove(product);
            productListLiveData.setValue(productList);
        }
    }

    public double getAmountSum() {
        List<Product> list = getProductListLiveData().getValue();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return (list != null) ?
                    list.stream().mapToDouble(Product::getQuantity).sum() : 0.0;
        } else {
            double sum = 0;
            for (Product product : list) {
                sum += product.getQuantity();
            }
            return sum;
        }
    }

    public int getListSize() {
        return (getProductListLiveData().getValue() != null) ?
                getProductListLiveData().getValue().size() : 0;
    }

    public void clearList() {
        productListLiveData.setValue(new ArrayList<>());
    }

    public void saveList(Context context) {
        SharedPrefUtil.saveUser(context, getProductListLiveData().getValue());
    }

    public void editProduct(Product oldProduct, Product newProduct) {
        List<Product> list = getProductListLiveData().getValue();
        if (list.contains(oldProduct)) {
            int index = list.indexOf(oldProduct);
            list.set(index, newProduct);
        }
        productListLiveData.setValue(list);
    }

    public void reorder(List<Product> list) {
        productListLiveData.setValue(list);
    }
}
