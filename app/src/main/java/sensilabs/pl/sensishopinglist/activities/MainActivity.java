package sensilabs.pl.sensishopinglist.activities;

import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import sensilabs.pl.sensishopinglist.R;
import sensilabs.pl.sensishopinglist.fragments.NewProductDialogFragment;
import sensilabs.pl.sensishopinglist.fragments.ProductListFragment;
import sensilabs.pl.sensishopinglist.fragments.ShopingListMenuFragment;
import sensilabs.pl.sensishopinglist.fragments.ShopingListSumFragment;

import static sensilabs.pl.sensishopinglist.uilts.ViewUtil.*;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        addFragment(this,R.id.sum_placeholder, new ShopingListSumFragment());
        addFragment(this,R.id.menu_placeholder, new ShopingListMenuFragment());
        addFragment(this,R.id.list_placeholder, new ProductListFragment());
        initFab();
    }

    private void initFab() {
        FloatingActionButton fab = findViewById(R.id.add_product_fab);
        fab.setOnClickListener(v -> openNewProductDialog());
    }

    private void openNewProductDialog() {
        NewProductDialogFragment dialog = new NewProductDialogFragment();
        dialog.show(getSupportFragmentManager(), "");
    }


}
