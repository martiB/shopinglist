package sensilabs.pl.sensishopinglist.adapters;

import android.app.Activity;
import android.app.AlertDialog;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.Collections;
import java.util.List;

import sensilabs.pl.sensishopinglist.R;
import sensilabs.pl.sensishopinglist.callbacks.ItemTouchHelperAdapter;
import sensilabs.pl.sensishopinglist.pojos.Product;
import sensilabs.pl.sensishopinglist.viewmodels.ShopingListVM;


public class ProductListAdapter extends RecyclerView.Adapter<ProductListAdapter.ViewHolder>
        implements ItemTouchHelperAdapter {

    private List<Product> productArrayList;
    private ShopingListVM shopingListVM;
    private Activity activity;


    @Override
    public boolean onItemMove(int fromPosition, int toPosition) {
        List<Product> list = shopingListVM.getProductListLiveData().getValue();
        if (fromPosition < toPosition) {
            for (int i = fromPosition; i < toPosition; i++) {
                Collections.swap(list, i, i + 1);
            }
        } else {
            for (int i = fromPosition; i > toPosition; i--) {
                Collections.swap(list, i, i - 1);
            }
        }
        shopingListVM.reorder(list);
        notifyItemMoved(fromPosition, toPosition);
        return true;
    }

    @Override
    public void onItemDismiss(int position) {
        shopingListVM.removeProduct(productArrayList.get(position));
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        TextView nr, itemName, quantity;
        ImageButton editButton, deleteButton;

        ViewHolder(View v) {
            super(v);
            nr = v.findViewById(R.id.nr);
            itemName = v.findViewById(R.id.name);
            quantity = v.findViewById(R.id.quantity);
            editButton = v.findViewById(R.id.edit_button);
            deleteButton = v.findViewById(R.id.delete_button);
        }
    }

    public ProductListAdapter(List<Product> productArrayList, ShopingListVM shopingListVM, FragmentActivity activity) {
        this.productArrayList = productArrayList;
        this.shopingListVM = shopingListVM;
        this.activity = activity;
    }

    @Override
    public ProductListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                            int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.product_item_layout, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ProductListAdapter.ViewHolder holder, int position) {
        holder.nr.setText(String.valueOf(position + 1));
        Product item = productArrayList.get(position);
        holder.itemName.setText(item.getName());
        holder.quantity.setText(String.valueOf(item.getQuantity()));
        holder.deleteButton.setOnClickListener(v -> shopingListVM.removeProduct(productArrayList.get(position)));
        holder.editButton.setOnClickListener(v -> showDialog(productArrayList.get(position)));
    }

    private void showDialog(Product item) {
        LayoutInflater inflater = activity.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.new_product_dialog, null);

        EditText name = dialogView.findViewById(R.id.product_name);
        name.setText(item.getName());

        EditText quantity = dialogView.findViewById(R.id.product_quantity);
        quantity.setText(String.valueOf(item.getQuantity()));

        hideButtons(dialogView);

        AlertDialog dialog = new AlertDialog.Builder(activity)
                .setTitle("Edycja")
                .setView(dialogView)
                .setNegativeButton("Anuluj", (dialog1, which) -> dialog1.cancel())
                .setPositiveButton("Zapisz",
                        (dialog1, which) -> {
                            String productName = name.getText().toString();
                            String productQuantity = quantity.getText().toString();
                            if (!productName.isEmpty() && !productQuantity.isEmpty()) {
                                shopingListVM.editProduct(item, new Product(productName, Double.valueOf(productQuantity)));
                            }
                        })
                .create();
        dialog.show();

    }

    private void hideButtons(View dialogView) {
        dialogView.findViewById(R.id.save_product_button).setVisibility(View.GONE);
        dialogView.findViewById(R.id.cancel_button).setVisibility(View.GONE);
    }

    @Override
    public int getItemCount() {
        return productArrayList.size();
    }
}



