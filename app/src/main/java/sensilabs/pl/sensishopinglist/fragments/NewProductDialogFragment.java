package sensilabs.pl.sensishopinglist.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.v7.app.AppCompatDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import sensilabs.pl.sensishopinglist.R;
import sensilabs.pl.sensishopinglist.pojos.Product;
import sensilabs.pl.sensishopinglist.viewmodels.ShopingListVM;

public class NewProductDialogFragment extends AppCompatDialogFragment {

    private Button saveButton, cancelButton;
    private Dialog dialog;
    private TextView productName, productQuantity;
    private ShopingListVM shopingListVM;


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        shopingListVM = ViewModelProviders.of(getActivity()).get(ShopingListVM.class);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.new_product_dialog, null);
        builder.setView(view);

        initTextFields(view);
        initSaveButton(view);
        initCancelButton(view);

        dialog = builder.create();
        return dialog;
    }

    private void initCancelButton(View view) {
        cancelButton = view.findViewById(R.id.cancel_button);
        cancelButton.setOnClickListener(v -> dialog.dismiss());
    }

    private void initSaveButton(View view) {
        saveButton = view.findViewById(R.id.save_product_button);
        saveButton.setOnClickListener(v -> saveProduct());
    }

    private void saveProduct() {
        String productNameText = productName.getText().toString();
        String productQuantityText = productQuantity.getText().toString();
        if (!productNameText.isEmpty() && !productQuantityText.isEmpty()) {
            Product product = new Product(productNameText, Double.valueOf(productQuantityText));
            shopingListVM.addProduct(product);
            dialog.dismiss();
        }
    }

    private void initTextFields(View view) {
        productName = view.findViewById(R.id.product_name);
        productQuantity = view.findViewById(R.id.product_quantity);
    }

}
