package sensilabs.pl.sensishopinglist.fragments;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import sensilabs.pl.sensishopinglist.R;
import sensilabs.pl.sensishopinglist.viewmodels.ShopingListVM;

public class ShopingListMenuFragment extends Fragment {

    private ShopingListVM shopingListVM;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frag_shopinglist_menu, container, false);

        shopingListVM = ViewModelProviders.of(getActivity()).get(ShopingListVM.class);

        initClearListButton(view);
        initSaveListButton(view);
        initLoadListButton(view);

        return view;
    }

    private void initLoadListButton(View view) {
        Button loadListButton = view.findViewById(R.id.load_list_button);
        loadListButton.setOnClickListener(v -> shopingListVM.getProductsFromSharePref(getActivity().getApplication()));
    }

    private void initSaveListButton(View view) {
        Button saveListButton = view.findViewById(R.id.save_list_button);
        saveListButton.setOnClickListener(v -> shopingListVM.saveList(getContext()));
    }

    private void initClearListButton(View view) {
        Button clearListButton = view.findViewById(R.id.clear_list_button);
        clearListButton.setOnClickListener(v -> shopingListVM.clearList());
    }
}
