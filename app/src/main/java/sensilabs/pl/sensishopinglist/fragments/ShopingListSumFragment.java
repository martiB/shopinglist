package sensilabs.pl.sensishopinglist.fragments;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import sensilabs.pl.sensishopinglist.R;
import sensilabs.pl.sensishopinglist.viewmodels.ShopingListVM;

public class ShopingListSumFragment extends Fragment {
    private TextView amount, listSize;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frag_shopinglist_sum, container, false);
        amount = view.findViewById(R.id.amount);
        listSize = view.findViewById(R.id.list_size);
        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ShopingListVM shopingListVM = ViewModelProviders.of(getActivity()).get(ShopingListVM.class);
        shopingListVM.getProductListLiveData().observe(getActivity(), list -> {
            amount.setText(String.valueOf(shopingListVM.getAmountSum()));
            listSize.setText(String.valueOf(shopingListVM.getListSize()));
        });
    }
}

