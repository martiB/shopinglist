package sensilabs.pl.sensishopinglist.fragments;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import sensilabs.pl.sensishopinglist.R;
import sensilabs.pl.sensishopinglist.adapters.ProductListAdapter;
import sensilabs.pl.sensishopinglist.callbacks.SimpleItemTouchCallback;
import sensilabs.pl.sensishopinglist.viewmodels.ShopingListVM;

public class ProductListFragment extends Fragment {

    private RecyclerView recyclerView;
    private ProductListAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frag_product_list, container, false);
        recyclerView = view.findViewById(R.id.product_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ShopingListVM shopingListVM = ViewModelProviders.of(getActivity()).get(ShopingListVM.class);
        shopingListVM.getProductListLiveData().observe(getActivity(), list -> {
            adapter = new ProductListAdapter(list, shopingListVM, getActivity());
            recyclerView.setAdapter(adapter);
            ItemTouchHelper.Callback callback = new SimpleItemTouchCallback(adapter);
            ItemTouchHelper touchHelper = new ItemTouchHelper(callback);
            touchHelper.attachToRecyclerView(recyclerView);
        });
    }
}
